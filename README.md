# ELASTIC STACK

Elastic Stack is a group of open source products from Elastic that were
designed to take data from any type of source and in any formats
and search, analyze, and visualize that data in almost a real time. 
The product group was formerly known as ELK Stack, 
in which the letters in the name stood for the products in the group: 
Elasticsearch, Logstash and Kibana.
Elastic company build few more products such as 
Beats and Xpack that required to change the name to Elastic stack.

## Elastic Stack components:

#### Elasticsearch
Elasticsearch is a RESTful distributed search engine built on top of 
Apache Lucene and released under an Apache license. It is Java-based and can 
search and index document files in diverse formats.

#### Logstash
Logstash is a data collection engine that unifies data from different sources, 
filters it and distributes it. The product was originally optimized for 
log data but has expanded the scope to take data from multiple sources, filter and 
enlarge the data and send it to many destinations using plugins.

#### Beats
Beats are “lightweight data shippers” that are installed on servers as agents
used to send different types of operational data to Elasticsearch either
directly or through Logstash, where the data might be enhanced or archived.

#### Kibana
Kibana is an open source data visualization and exploration tool from that is 
specialized for large volumes of streaming and real-time data. 
The software makes huge and complex data streams more easily and quickly 
understandable through graphic representation.

## Elastic Stack Installation
All the products can be installed using your preferred os installer or via
containers (We will use the containers approach).

#### Elastic Stack structure
**NOTES:** 
- All the products bellow were set in a separate container per product.
- Each container run on a dedicated host (vm)
- Every host has 2 interfaces (internal, external)

```
Filebeat --> Logstash1 ---> Elasticsearch Master <-- Kibana
        |                ->       |
        |               |         |
         ->  Logstash2 --         |
                            Elasticsearch Data1
                                  |
                                  |
                            Elasticsearch Data2
```

#### Elasticsearch installation

* download elasticsearch container
```
docker pull docker.elastic.co/elasticsearch/elasticsearch:6.2.3
```

* update the vm.max_map_count parameter in /etc/sysctl.conf file
```
vm.max_map_count=262144
```

* reload sysctl configuration
```
sysctl -p --system
```

* open the following port on all elasticsearch nodes (master, data)
```
firewall-cmd --add-port=4789/udp --permanent
firewall-cmd --add-port=7946/udp --permanent
firewall-cmd --add-port=7946/tcp --permanent
firewall-cmd --add-port=2377/tcp --permanent
firewall-cmd --reload
```

* initialize a swarm on the elasticsearch master node

**NOTE:** on node with more than 2 interfaces we have to include the --advertise-addr flag)
```
docker swarm init --advertise-addr <elasticsearch-master node internal ip>
```

NOTE: The result of the above command contains the token that we will use to join elasticsearch-data1 and elasticsearch-data2 to the swarm

* create an overlay network on elasticsearch master node
```
docker network create \
--driver=overlay \
--attachable \
elasticstack-net
```

* we need to connect every elasticsearch data node to the overlay network 
```
docker swarm join \
--token SWMTKN-1-19c3qbhknaf5i3b2n0cuhzdx9zohnpt33miervza3h5hldbpsu-2959qifu92qblhj7ko47xpc4y \
--advertise-addr <elasticsearch-data[1,2] node internal ip> \
elasticsearch.example.com:2377
```

* create /etc/elasticsearch/ directory
```
mkdir /etc/elasticsearch
```

* copy the relevant elasticsearch.yml to /etc/elasticsearch/

**master node**
```
cp ./elasticsearch/master/elasticsearch.yml /etc/elasticsearch/
```

**data node**
```
cp ./elasticsearch/data/elasticsearch.yml /etc/elasticsearch/
```

* run the elasticsearch container
```
docker run \
--detach \
--publish 9200:9200 \
--publish 9300:9300 \
--name elasticsearch \
--restart always \
--volume /etc/elasticsearch/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml \
--network elasticstack-net \
docker.elastic.co/elasticsearch/elasticsearch:6.2.3
```

#### Logstash installation

* download logstash container
```
docker pull docker.elastic.co/logstash/logstash:6.2.3
```

* create /etc/logstash/ directory
```
mkdir /etc/logstash
```

* copy the logstash directory content to /etc/
```
cp -r ./logstash/* /etc/logstash/
```

* run the logstash container
```
docker run \
--detach \
--publish 9600:9600 \
--publish 5044:5044 \
--name logstash \
--restart always \
-v /etc/logstash/logstash.yml:/usr/share/logstash/config/logstash.yml \
-v /etc/logstash/pipeline:/usr/share/logstash/pipeline \
docker.elastic.co/logstash/logstash:6.2.3
```

#### Kibana installation

* download kibana container
```
docker pull docker.elastic.co/kibana/kibana:6.2.3
```

* create /etc/kibana/ directory
```
mkdir /etc/kibana
```

* copy the kibana.yml to /etc/kibana/
```
cp -r ./kibana/kibana.yml /etc/kibana/
```

* run the kibana container
```
docker run \
--detach \
--publish 5601:5601 \
--name kibana \
--restart always \
-v /etc/kibana/kibana.yml:/usr/share/kibana/config/kibana.yml \
docker.elastic.co/kibana/kibana:6.2.3
```

## Beats installation

#### Filebeat

**NOTE:** The filebeat container should be installed on any node
that should ship logs to logstash

* download filebeat container
```
docker pull docker.elastic.co/beats/filebeat:6.2.3
```

* create /etc/filebeat/ directory
```
mkdir /etc/filebeat
```

* copy the filebeat.yml to /etc/filebeat/
```
cp ./filebeat/filebeat.yml /etc/filebeat/
```

* run the filebeat container
```
docker run \
--detach \
--name filebeat \
--hostname $HOSTNAME \
--restart always \
-v /etc/filebeat/filebeat.yml:/usr/share/filebeat/filebeat.yml \
-v /var/log:/var/log \
-u root \
docker.elastic.co/beats/filebeat:6.2.3
```

* install template to elasticsearch
```
docker run docker.elastic.co/beats/filebeat:6.2.3 setup \
--template \
-E output.logstash.enabled=false \
-E 'output.elasticsearch.hosts=["elasticsearch.example.com:9200"]'
```

* setup kibana dashboards
```
docker run docker.elastic.co/beats/filebeat:6.2.3 setup --dashboards
```

#### Metricbeat

**NOTES:**
- The metricbeat container should be installed on any node
- The metrics that will be sent includes memory, disk, and cpu usage.

* download metricbeat container
```
docker pull docker.elastic.co/beats/metricbeat:6.2.3
```

* setup kibana dashboards
```
docker run docker.elastic.co/beats/metricbeat:6.2.3 setup \
-E setup.kibana.host=kibana.example.com:5601 \
-E output.elasticsearch.hosts=["elasticsearch.example.com:9200"]
```

* create /etc/metricbeat/ directory
```
mkdir /etc/metricbeat
```

* copy the metricbeat.yml to /etc/metricbeat/
```
cp ./metricbeat/metricbeat.yml /etc/metricbeat/
```

# run the metricbeat container
```
docker run \
--detach \
--name metricbeat \
--restart always \
--hostname $HOSTNAME \
-v /etc/metricbeat/metricbeat.yml:/usr/share/metricbeat/metricbeat.yml:ro \
-v /var/run/docker.sock:/var/run/docker.sock:ro \
-v /sys/fs/cgroup:/hostfs/sys/fs/cgroup:ro \
-v /proc:/hostfs/proc:ro \
-v /:/hostfs:ro \
-u root \
docker.elastic.co/beats/metricbeat:6.2.3
```

#### Packetbeat

* download packetbeat container
```
docker pull docker.elastic.co/beats/packetbeat:6.2.3
```

* setup kibana dashboards
```
docker run docker.elastic.co/beats/packetbeat:6.2.3 setup \
-E setup.kibana.host=kibana.example.com:5601 \
-E output.elasticsearch.hosts=["elasticsearch.example.com:9200"]
```

* create /etc/packetbeat/ directory
```
mkdir /etc/packetbeat
```

* copy the packetbeat.yml to /etc/packetbeat/
```
cp ./packetbeat/packetbeat.yml /etc/packetbeat/
```


* run the packetbeat container
```
docker run \
--detach \
--name=packetbeat \
--user=root \
--restart always \
--hostname $HOSTNAME \
-v /etc/packetbeat/packetbeat.yml:/usr/share/packetbeat/packetbeat.yml:ro \
--cap-add="NET_RAW" \
--cap-add="NET_ADMIN" \
--network=host \
docker.elastic.co/beats/packetbeat:6.2.3
```